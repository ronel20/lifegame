import java.util.Random;

/**
 * This class represents the Logic behind the Life Game
 *
 * @author Ronel David
 */
public class LifeGameLogic {


    private boolean[][] _lifeMatrix;

    public boolean[][] get_lifeMatrix() {
        return _lifeMatrix;
    }

    /**
     * Constructor
     * @param squareInWidth number of squares on window width
     * @param squareInHeight number of squares on window height
     */
    public LifeGameLogic(int squareInWidth,int squareInHeight) {
        this._lifeMatrix=new boolean[squareInWidth][squareInHeight];
        Random rnd=new Random();
        for (int i=0;i<this._lifeMatrix.length;i++)
        {
            for (int j=0;j<this._lifeMatrix[i].length;j++)
            {
                if(rnd.nextBoolean()==true)
                {
                    this._lifeMatrix[i][j]=true;
                }
            }
        }
    }

    /**
     * checks if number of neighbors qualify for new cell to be born
     * @param livingNeighbors number of living neighbors
     * @return true if new cell born conditions are met, false otherwise
     */
    public boolean willBorn(int livingNeighbors)
    {
        if (livingNeighbors==3)
        {
            return true;
        }
        return false;
    }

    /**
     * checks if number of neighbors qualify for the death of an existing cell
     * @param livingNeighbors number of living neighbors
     * @return true if death of an existing cell conditions are met, false otherwise
     */
    public boolean willDie(int livingNeighbors)
    {
        if (livingNeighbors>=4 || livingNeighbors<=1)
        {
            return true;
        }
        return false;
    }

    /**
     * gets the number of living neighbors of a given cell
     * @param coordinateX given cell x position
     * @param coordinateY given cell y position
     * @return number of living neighbors
     */
    public int getLivingNeighbors(int coordinateX,int coordinateY)
    {
        int neighborsCount=0;
        //loop through neighbors
        for (int i=coordinateX-1;i<=coordinateX+1;i++)
        {
            for (int j=coordinateY-1;j<=coordinateY+1;j++)
            {
                //matrix index out of bounds and not counting cell itself condition
                if (i<0 || i>this._lifeMatrix.length-1 || j<0 || j>this._lifeMatrix[i].length-1 || (i==coordinateX && j==coordinateY))
                {
                    continue;
                }
                if(this._lifeMatrix[i][j]==true)
                {
                    neighborsCount++;
                }
            }
        }
        return neighborsCount;
    }

    /**
     * generates the next generation of cell according to John Conway model
     */
    public void generateNextGen()
    {
        boolean[][] nextGenMatrix=new boolean[this._lifeMatrix.length][this._lifeMatrix[0].length];
        for (int i=0;i<this._lifeMatrix.length;i++)
        {
            for (int j=0;j<this._lifeMatrix[i].length;j++)
            {
                int numOfNeighbors= this.getLivingNeighbors(i,j);
                //die condition
                if (this._lifeMatrix[i][j]==true && this.willDie(numOfNeighbors))
                {
                    nextGenMatrix[i][j]=false;
                }
                //born condition
                else if (this._lifeMatrix[i][j]==false && this.willBorn(numOfNeighbors))
                {
                    nextGenMatrix[i][j]=true;
                }
                //exist condition
                else
                {
                    nextGenMatrix[i][j]=this._lifeMatrix[i][j];
                }
            }
        }
        this._lifeMatrix=nextGenMatrix;
    }
}
