import javax.swing.*;

public class Play {

    private static final int MATRIX_WIDTH=10;
    private static final int MATRIX_HEIGHT=10;

    public static void main(String[] args)
    {
        JFrame window = new JFrame();
        LifeGameLogic logic=new LifeGameLogic(MATRIX_WIDTH,MATRIX_HEIGHT);
        LifeGamePanel lifeGameDrawing= new LifeGamePanel(logic.get_lifeMatrix());
        window.add(lifeGameDrawing);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setSize(MATRIX_WIDTH*80+18,MATRIX_HEIGHT*80+47);
        window.setResizable(false);
        window.setTitle("Life Game");
        window.setVisible(true);

        //while user answers yes
        while(JOptionPane.showConfirmDialog(window,"Generate next Genration?","Life Game",JOptionPane.YES_NO_CANCEL_OPTION)==JOptionPane.YES_OPTION)
        {
            logic.generateNextGen();
            lifeGameDrawing.setLifeMatrix(logic.get_lifeMatrix());
            lifeGameDrawing.repaint();
        }

    }
}
