import javax.swing.*;
import java.awt.*;

/**
 * This class represents the visual part of the Life Game app
 *
 * @author Ronel David
 */
public class LifeGamePanel extends JPanel {

    private boolean[][] _lifeMatrix;

    /**
     * Constructor
     * @param _lifeMatrix matrix representing a life game
     */
    public LifeGamePanel(boolean[][] _lifeMatrix) {
        this._lifeMatrix = _lifeMatrix;
    }

    /**
     * paints the Life Game board on the app window
     * @param g Graphics class instance
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        int width = this.getWidth();
        int height = this.getHeight();
        int x1=0;
        int y1=0;
        for (;x1<this._lifeMatrix.length;x1++,y1=0)
        {
            for (;y1<this._lifeMatrix[x1].length;y1++)
            {
                if (this._lifeMatrix[x1][y1]==true)
                {
                    g.setColor(new Color(0,255,150));
                    g.fillRect(x1*(width/this._lifeMatrix.length),y1*(height/this._lifeMatrix[x1].length),width/_lifeMatrix.length,height/_lifeMatrix[x1].length);
                    g.setColor(new Color(0,0,0));
                }
                g.drawRect(x1*(width/_lifeMatrix.length),y1*(height/_lifeMatrix[x1].length),width/_lifeMatrix.length,height/_lifeMatrix[x1].length);
            }
        }
    }

    /**
     * sets the life matrix
     * @param _lifeMatrix The new Life Game matrix
     */
    public void setLifeMatrix(boolean[][] _lifeMatrix) {
        this._lifeMatrix = _lifeMatrix;
    }
}
